import * as THREE from './three.module.js';


function resizeRendererToDisplaySize(renderer) {
    const canvas = renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.width !== width || canvas.height !== height;
    if (needResize) {
        renderer.setSize(width, height, false);
    }
    return needResize;
}

function addLighting(scene) {
    const color = 0xFFFFFF;
    const intensity = 1;
    const light = new THREE.DirectionalLight(color, intensity);
    light.position.set(-1, 2, 4);
    scene.add(light);
}

function addCenterObject(scene) {
    const geometry = new THREE.BoxGeometry();
    const material = new THREE.MeshPhongMaterial({color: 0x00ff00});
    //const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
    const cube = new THREE.Mesh( geometry, material );

    scene.add( cube );

    return cube;
}

function addParticles(scene, sprite, colorblend, vdepth = 100, vsize = 2) {
    // create the particle variables
    let particleCount = 1000;
    let geometry = new THREE.BufferGeometry();
    let radius = 5.0;
    let particleSystem;

    const loader = new THREE.TextureLoader();
    const cross = loader.load(sprite);

    const material = new THREE.PointsMaterial( { color: colorblend, size: vsize, map: cross, transparent: true, blending: THREE.AdditiveBlending} );

    const positions = [];
    const colors = [];
    const sizes = [];

    const color = new THREE.Color();

    for ( let i = 0; i < particleCount; i ++ ) {

        var x = Math.random()*2-1;
        var y = Math.random()*2-1;
        var z = Math.random()*2-1;
        let rsqr = Math.sqrt(x*x+y*y+z*z);

        x /= rsqr;
        y /= rsqr;
        z /= rsqr;

        let depth = (Math.random() + 1) * vdepth;

        positions.push( x * (radius * depth));
        positions.push( y * (radius * depth));
        positions.push( z * (radius * depth));
    }

    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( positions, 3 ) );
   
    particleSystem = new THREE.Points( geometry, material );

    scene.add( particleSystem );

    return particleSystem;
}

function main() {
    const canvas = document.querySelector('#c');
    const renderer = new THREE.WebGLRenderer({canvas});

    const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 10000 );
    const scene = new THREE.Scene();

    addLighting(scene);
    const cube = addCenterObject(scene);
    let red =  new THREE.Color(0xA58581);
    let blue = new THREE.Color(0x8791A5);
    let white = new THREE.Color(0x929292);
    let particlesSystems = []
    particlesSystems.push(addParticles(scene,'./static/Cross.png', red));
    particlesSystems.push(addParticles(scene,'./static/Cross.png', blue));
    particlesSystems.push(addParticles(scene,'./static/Cross.png', white));
    particlesSystems.push(addParticles(scene,'./static/Dot.png', red));
    particlesSystems.push(addParticles(scene,'./static/Dot.png', blue));
    particlesSystems.push(addParticles(scene,'./static/Dot.png', white));
    camera.position.z = 5;

    document.addEventListener( 'mousewheel', (event) => {
        cube.rotation.x += event.deltaY/500;
        cube.rotation.y += event.deltaY/500;
        cube.rotation.z += event.deltaY/500;
        for (let i = 0; i < particlesSystems.length; i++) {
            particlesSystems[i].rotation.x += event.deltaY/500;
             particlesSystems[i].rotation.y += event.deltaY/500;
             particlesSystems[i].rotation.z += event.deltaY/500;
        }
    });

    const animate = function () {

        if (resizeRendererToDisplaySize(renderer)) {
            const canvas = renderer.domElement;
            camera.aspect = canvas.clientWidth / canvas.clientHeight;
            camera.updateProjectionMatrix();
        }

        renderer.render( scene, camera );

        requestAnimationFrame( animate );
    };

    animate();
}

main();